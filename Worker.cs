using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using NATS.Client;
using Newtonsoft.Json;

namespace nats_dotnet_requestor
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        ConnectionFactory cf;
        ISyncSubscription sSync;
        IConnection c;

        String subject = EnvConfigs.NATS_SUBJECT;

        Byte[] data = null;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        public override async Task StartAsync(CancellationToken cancellationToken)
        {
            cf = new ConnectionFactory();
            var options = ConnectionFactory.GetDefaultOptions();
            options.Url = EnvConfigs.NATS_URL;
            c = cf.CreateConnection(options);
            await base.StartAsync(cancellationToken);
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stopping...");
            await c.DrainAsync(1000);
            await base.StopAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation($"Requester worker has started running at: {DateTimeOffset.Now}");

            while (!stoppingToken.IsCancellationRequested)
            {

                try
                {
                    _logger.LogInformation("Requesting on foo.request");
                    var m = await c.RequestAsync(subject, data, EnvConfigs.NATS_TIMEOUT);
                    _logger.LogInformation("Received information...");
                    _logger.LogInformation("Data: " + m.ToString());
                }
                catch (NATSTimeoutException)
                {
                    _logger.LogInformation($"Timed out awaiting response at: {DateTimeOffset.Now}");
                }

                _logger.LogInformation($"Going to sleep for {EnvConfigs.LOOP_TIMEOUT}");
                await Task.Delay(EnvConfigs.LOOP_TIMEOUT, stoppingToken);
            }


        }


    }
}
