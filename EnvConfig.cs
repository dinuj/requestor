using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace nats_dotnet_requestor
{
    public static class EnvConfigs
    {
        public static string NATS_URL = Environment.GetEnvironmentVariable("NATS_URL");
        public static string NATS_SUBJECT = Environment.GetEnvironmentVariable("NATS_SUBJECT");
        public static string NATS_Q = Environment.GetEnvironmentVariable("NATS_Q");
        public static int NATS_TIMEOUT = Convert.ToInt32(Environment.GetEnvironmentVariable("NATS_TIMEOUT"));
        public static int LOOP_TIMEOUT = Convert.ToInt32(Environment.GetEnvironmentVariable("LOOP_TIMEOUT"));
        
        
    }
}
